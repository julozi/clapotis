---
title: Les bébés nageurs
type: page
---

Lors d'une séance, différentes activités vous sont proposées par l'équipe d'animateurs qui vous guide et vous conseille.

L'activité doit être un moment ludique, un moment de détente et un moment privilégié avec votre enfant.

Le bassin est aménagé avec du matériel ludique en fonction de l'âge des enfants et en respectant le rythme de chacun.

## Conditions générales

* Age :	pour les enfants de 4 mois à 6 ans
* Vaccinations :	Deux vaccins DTPolio
* Certificat médical attestant de l'absence de contre-indication à la pratique de l'activité (l'enfant ne pourra être admis sans certificat)
* Avec qui :	accompagné d'un ou deux adultes
* Collation :	l'enfant doit avoir pris une collation avant la séance et il faut prévoir un goûter (à mettre dans un récipient en plastique) à la sortie de l'eau.

## Horaires

Samedi entre 12h et 14h :

* de 4 mois à 15 mois : 12h à 12h30
* de 15 mois à 3 ans et famille : 12h30 à 13h10
* de 3 ans à 6 ans et famille : 13h10 à 13h50

## Conditions de l'activité

* eau chaude (env. 31°c)
* bassin ludique
* encadrement par des animateurs formés Faael
* surveillance du bassin par un BNSSA
* durée des séances :
     30 minutes pour les moins de 15 mois
     40 minutes pour les plus de 15 mois

## Hygiène

* Douche obligatoire pour les adultes
* Pas de caleçon de bain pour les hommes
* Slip de bain ou couche spéciale piscine pour l'enfant
* NE PAS VENIR à la séance avec un enfant malade
