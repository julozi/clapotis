---
title: L'école de natation
type: page
---

L'enseignement de l'école de natation repose sur l'apprentissage des quatre nages codifiées :

* crawl
* dos crawlé
* brasse
* papillon

## Informations et conditions générales

* avoir au moins 6 ans
* séances le mercredi entre 12h30 et 14h30
* séances de 40 minutes
* entre 12 et 15 enfants par groupe
* cycle annuel de 24 séances
* enseignement dispensé par des MNS ou BEESAN dans le bassin sportif

## Hygiène

* Douche obligatoire
* Pas de caleçon de bain

<div class="alert alert-warning" role="alert">
Dorénavant les parents ne peuvent plus accompagner leur enfant au bord du bassin.

Nous organisons deux séances pour aller voir l'évolution de vos enfants au bassin : uniquement en tenue de bain.

Merci de votre compréhension.
</div>
